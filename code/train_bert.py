import torch

import constants
import run_bert as rb
import pandas as pd
import json
from transformers import BertTokenizer
import utils
import bert_tokenizer as bt
import wandb

def tokenize_input(csv_path, out_path):
    tokenizer = BertTokenizer.from_pretrained('bert-base-german-cased')
    impressions = bt.get_impressions_from_csv(csv_path)
    new_impressions = bt.tokenize(impressions, tokenizer)
    with open(out_path, 'w') as filehandle:
            json.dump(new_impressions, filehandle)

def split_train_data(csv_path):
    df = pd.read_csv(csv_path)
    train_df = df.sample(frac=0.8)
    val_df = df.drop(train_df.index)
    train_csv_path = csv_path.parent / 'train.csv'
    train_df.to_csv(train_csv_path, index=False)

    val_csv_path = csv_path.parent / 'val.csv'
    val_df.to_csv(val_csv_path, index=False)

def train(input_folder_path, out_path):
    train_csv_path = input_folder_path / 'train.csv'
    dev_csv_path = input_folder_path / 'val.csv'
    train_imp_path = input_folder_path / 'tokenized_train.csv'
    dev_imp_path = input_folder_path / 'tokenized_val.csv'

    tokenize_input(train_csv_path, train_imp_path)
    tokenize_input(dev_csv_path, dev_imp_path)


    f1_weights = utils.get_weighted_f1_weights(dev_csv_path.as_posix())

    dataloaders = rb.load_data(train_csv_path, train_imp_path, dev_csv_path, dev_imp_path)
    wandb.init(
     project="CXR_Report_Labeler",
     name="model_training",
     config={
     "architecture": constants.PRETRAIN_PATH
     }
     )

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    rb.train(save_path=out_path,
           dataloaders=dataloaders,
           model=None,
           optimizer=None,
           device=device,
           f1_weights=f1_weights
             )
