from pathlib import Path

import label
from train_bert import train

if __name__ == "__main__":
    mode = "training"
    input_folder_path = Path("../training_input")  # The path containing the train and validation data
    Path("../output").mkdir(exist_ok=True, parents=True)
    output_folder_path = "../output"
    if mode == "training":
        train(input_folder_path, output_folder_path)
    elif mode == "labeling":
        input_path = '../training_input/test.csv'  # The path containing the reports to be labeled
        output_path = '../output/'
        checkpoint_path = '../output/saved_model.pth'
        y_pred = label.label(checkpoint_path, input_path)
        label.save_preds(y_pred, input_path, output_path)
    else:
        raise ValueError("Invalid mode")
