# Language model-based labeling of German thoracic radiology reports

Code for the paper "Language model-based labeling of German thoracic radiology reports
", by Wollek et al., Fortschr Röntgenstr 2024; 1–10

## Description
This repository contains a specialized tool for automatically extracting annotations from German thoracic radiology
reports. It is based on the CheXbert architecture(code an be found [here](https://github.com/stanfordmlgroup/CheXbert)). The underlying methodology and logic are comprehensively detailed in our accompanying research paper, which
can be accessed here [ArXiv](https://arxiv.org/abs/2306.05997).

## Installation
To install the necessary dependencies for this project, use the following command: `conda env create -f environment.yml
`

## Usage
To utilize this tool, you should first configure the file paths in `code/main.py`. Detailed comments are provided
within the file to guide you in understanding the purpose of each path setting. 


## License
MIT
